import discord
import os

from dotenv import load_dotenv
load_dotenv()

DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

print("Bismillahirrahmanirrahim")

intents = discord.Intents.all()
bot = discord.Client(intents=intents)

@bot.event
async def on_ready():
  print("bot hazır!")
  print(f"We have logged in as {bot.user}")

@bot.event
async def on_message(message):

  # kendi yazdığımız mesajları görmezden gelelim
  if message.author == bot.user:
    print("kendi mesajım RETURN")
    return

  if message.content == "!ping":
    await message.channel.send("!pong")

  elif message.content.startswith('!challenge'):
    await message.channel.send("kapışalım")

  elif message.content.startswith('!leaderboard'):
    await message.channel.send("liderim")

  elif message.content == "selam":
    await message.channel.send("kelam")

bot.run(DISCORD_TOKEN)