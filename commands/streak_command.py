# commands/streak_command.py
from datetime import datetime, timedelta
import json
import discord
import asyncio
import requests
import os

async def send_congratulatory_message(message, congratulatory_message):
    await message.channel.send(congratulatory_message)

async def send_specific_image(message, image_url):
    # Download the image from the URL and save it locally
    response = requests.get(image_url)
    if response.status_code == 200:
        with open("JM.png", "wb") as f:
            f.write(response.content)

        # Send the image from the local file
        await message.channel.send(file=discord.File("JM.png"))

        # Clean up the local file after sending
        if os.path.exists("JM.png"):
            os.remove("JM.png")
    else:
        await message.channel.send("Failed to fetch the image.")

def update_streaks(message):
    user_id = str(message.author.id)
    today_date = datetime.now().date()
    with open("storage/scores.json", "r") as f:
        streaks = json.load(f)

    if user_id not in streaks:
        streaks[user_id] = {
            'streak': 1,
            'last_login': str(today_date)
        }

    last_login = datetime.strptime(streaks[user_id]['last_login'], '%Y-%m-%d').date()

    if last_login == today_date - timedelta(days=1): # en son dün rıtin yaptıysa +1
        streaks[user_id]['streak'] += 1
    elif last_login < today_date - timedelta(days=1): # dün rutin yapmadıysa sıfırla
        streaks[user_id]['streak'] = 1

    if streaks[user_id]['streak'] == 40:  # Check if the streak is 40
        congratulatory_message = f"Congratulate {message.author.mention} on his 40-day streak!"
        image_url = "https://cdn.discordapp.com/attachments/770214710342844456/1132037233893969950/JM.png"

        asyncio.create_task(send_congratulatory_message(message, congratulatory_message))
        asyncio.create_task(send_specific_image(message, image_url))

    streaks[user_id]['last_login'] = str(today_date)

    with open("storage/scores.json", "w") as f:
        json.dump(streaks, f)

async def update_streaks_async(message):
    update_streaks(message)
