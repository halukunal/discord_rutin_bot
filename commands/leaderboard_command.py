# commands/leaderboard_command.py
import json
import discord

async def leaderboard_command(bot, message):
    with open("storage/scores.json", "r") as f:
        scores = json.load(f)

    leaderboard = sorted(scores.items(), key=lambda x: x[1]['streak'], reverse=True)
    leaderboard_str = "Leaderboard:\n"
    for idx, (user_id, data) in enumerate(leaderboard, 1):
        user = await bot.fetch_user(int(user_id))
        leaderboard_str += f"{idx}. {user.name}: {data['streak']} days\n"

    await message.channel.send(leaderboard_str)
