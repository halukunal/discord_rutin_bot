import json
import random
import discord
from discord.ext import commands

async def start_challenge(ctx):
    with open("storage/challenges.json", "r") as f:
        challenges_data = json.load(f)

    # Select 3 random challenges and authors
    selected_challenges = random.sample(challenges_data, 3)

    challenge_message = ""
    for idx, challenge in enumerate(selected_challenges, 1):
        challenge_message += f"{idx}. {challenge['challenge']} - {challenge['author']}\n"

    options = ["😀", "😉", "😎"]

    # Send challenge questions
    challenge_msg = await ctx.channel.send(challenge_message)

    # Add reactions as options
    for option in options:
        await challenge_msg.add_reaction(option)

# No need to run the bot here, it will be run in main.py
