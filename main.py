import discord
import os
from commands import streak_command, leaderboard_command, challenges
import json

intents = discord.Intents.all()
bot = discord.Client(intents=intents)

from dotenv import load_dotenv
load_dotenv()
DISCORD_TOKEN = os.getenv("DISCORD_TOKEN")

@bot.event
async def on_ready():
    print(f"We have logged in as {bot.user}")

@bot.event
async def on_message(message):
    if message.author == bot.user:
        return

    if message.content.startswith('!rutin'):
        await streak_command.update_streaks_async(message)
        user_id = str(message.author.id)
        with open("storage/scores.json", "r") as f:
            streaks = json.load(f)

        await message.channel.send(f"Your current streak is {streaks[user_id]['streak']}")

    if message.content.startswith('!leaderboard'):
        await leaderboard_command.leaderboard_command(bot, message)

    if message.content.startswith('!challenge'):
        await challenges.start_challenge(message)


# Replace 'YOUR_BOT_TOKEN' with your actual bot token
#bot.run(os.environ['TOKEN'])
bot.run(DISCORD_TOKEN)